package com.bala.bank.service.impl;

import java.math.BigDecimal;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.bala.bank.exception.AccountNotExistException;
import com.bala.bank.exception.ApplicationException;
import com.bala.bank.model.Account;
import com.bala.bank.repository.AccountRepository;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceImplTest {

	@Mock
	private AccountRepository accountRepository;

	@InjectMocks
	private AccountServiceImpl accountService;

	@Test
	public void testSaveAccount() {
		Account accountOne = new Account();
		accountOne.setAccountNumber(1L);
		accountOne.setAmount(new BigDecimal(20000));

		Mockito.when(accountRepository.findById(Mockito.anyLong())).thenReturn(Optional.empty());

		Mockito.when(accountRepository.save(accountOne)).thenReturn(accountOne);

		Account result = accountService.saveAccount(accountOne);

		Assert.assertEquals(accountOne.getAmount(), result.getAmount());
	}

	@Test(expected = ApplicationException.class)
	public void testSaveAccountApplicationException() {
		Account accountOne = new Account();
		accountOne.setAccountNumber(1L);
		accountOne.setAmount(new BigDecimal(20000));

		Mockito.when(accountRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(accountOne));

		accountService.saveAccount(accountOne);

	}

	@Test
	public void testUpdateBalance() {
		Account accountOne = new Account();
		accountOne.setAccountNumber(1L);
		accountOne.setAmount(new BigDecimal(20000));

		Mockito.when(accountRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(accountOne));

		Account account = new Account();
		account.setAmount(new BigDecimal(30000));

		Mockito.when(accountRepository.save(accountOne)).thenReturn(accountOne);
		Account result = accountService.updateBalance(1L, account);

		Assert.assertEquals(result.getAmount(), new BigDecimal(50000));
	}

	@Test(expected = AccountNotExistException.class)
	public void testUpdateBalanceAccountNotExist() {

		Mockito.when(accountRepository.findById(Mockito.anyLong())).thenReturn(Optional.empty());

		Account account = new Account();
		account.setAmount(new BigDecimal(30000));

		accountService.updateBalance(1L, account);

	}

	@Test
	public void testGetBalance() {
		Account accountOne = new Account();
		accountOne.setAccountNumber(1L);
		accountOne.setAmount(new BigDecimal(20000));

		Mockito.when(accountRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(accountOne));

		Account result = accountService.getBalance(1L);

		Assert.assertEquals(result.getAmount(), accountOne.getAmount());
	}

	@Test(expected = AccountNotExistException.class)
	public void testGetBalanceNotExist() {

		Mockito.when(accountRepository.findById(Mockito.anyLong())).thenReturn(Optional.empty());
		accountService.getBalance(1L);

	}

}
