package com.bala.bank.service.impl;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.bala.bank.exception.InsufficientBalanceException;
import com.bala.bank.exception.TransactionNotExistException;
import com.bala.bank.model.Account;
import com.bala.bank.model.Transaction;
import com.bala.bank.repository.AccountRepository;
import com.bala.bank.repository.TransactionalRepository;

@RunWith(MockitoJUnitRunner.class)
public class TransactionalServiceImplTest {

	@Mock
	private AccountRepository accountRepository;

	@Mock
	private TransactionalRepository transactionalRepository;

	@InjectMocks
	private TransactionalServiceImpl transactionService;

	@Test
	public void testMoneyTransfer() {

		Account accountOne = new Account();
		accountOne.setAccountNumber(1L);
		accountOne.setAmount(new BigDecimal(20000));

		Account accountTwo = new Account();
		accountTwo.setAccountNumber(2L);
		accountTwo.setAmount(new BigDecimal(10000));

		Transaction transaction = new Transaction();
		transaction.setSourceAccountNumber(accountOne.getAccountNumber());
		transaction.setDestinationAccountNumber(accountTwo.getAccountNumber());
		transaction.setAmount(new BigDecimal(5000));

		Mockito.when(accountRepository.findById(transaction.getSourceAccountNumber()))
				.thenReturn(Optional.of(accountOne));
		Mockito.when(accountRepository.findById(transaction.getDestinationAccountNumber()))
				.thenReturn(Optional.of(accountTwo));

		Mockito.when(accountRepository.save(accountOne)).thenReturn(accountOne);
		Mockito.when(accountRepository.save(accountTwo)).thenReturn(accountTwo);

		transactionService.moneyTransfer(transaction);

		Assert.assertEquals(accountOne.getAmount(), new BigDecimal(15000));
		Assert.assertEquals(accountTwo.getAmount(), new BigDecimal(15000));
	}

	@Test(expected = InsufficientBalanceException.class)
	public void testMoneyTransferInsufficientBalance() {

		Account accountOne = new Account();
		accountOne.setAccountNumber(1L);
		accountOne.setAmount(new BigDecimal(10000));

		Account accountTwo = new Account();
		accountTwo.setAccountNumber(2L);
		accountTwo.setAmount(new BigDecimal(10000));

		Transaction transaction = new Transaction();
		transaction.setSourceAccountNumber(accountOne.getAccountNumber());
		transaction.setDestinationAccountNumber(accountTwo.getAccountNumber());
		transaction.setAmount(new BigDecimal(15000));

		Mockito.when(accountRepository.findById(transaction.getSourceAccountNumber()))
				.thenReturn(Optional.of(accountOne));
		Mockito.when(accountRepository.findById(transaction.getDestinationAccountNumber()))
				.thenReturn(Optional.of(accountTwo));

		transactionService.moneyTransfer(transaction);
	}

	@Test
	public void testGetTransaction() {
		Transaction transaction = new Transaction();
		transaction.setTransactionId(1L);
		transaction.setSourceAccountNumber(1L);
		transaction.setDestinationAccountNumber(2L);
		transaction.setAmount(new BigDecimal(15000));
		transaction.setDateTime(new Timestamp(System.currentTimeMillis()));

		Mockito.when(transactionalRepository.findById(transaction.getTransactionId()))
				.thenReturn(Optional.of(transaction));

		transactionService.getTransaction(transaction.getTransactionId());

		Assert.assertNotNull(transaction);
	}

	@Test(expected = TransactionNotExistException.class)
	public void testGetTransactionNotExist() {

		Mockito.when(transactionalRepository.findById(Mockito.anyLong())).thenReturn(Optional.empty());
		transactionService.getTransaction(1L);

	}

	@Test
	public void testGetAllTransactionByAccountNumber() {
		List<Transaction> transactionList = new ArrayList<Transaction>();

		Transaction transaction = new Transaction();
		transaction.setTransactionId(1L);
		transaction.setSourceAccountNumber(1L);
		transaction.setDestinationAccountNumber(2L);
		transaction.setAmount(new BigDecimal(15000));
		transaction.setDateTime(new Timestamp(System.currentTimeMillis()));

		Transaction transactionTwo = new Transaction();
		transactionTwo.setTransactionId(2L);
		transactionTwo.setSourceAccountNumber(1L);
		transactionTwo.setDestinationAccountNumber(2L);
		transactionTwo.setAmount(new BigDecimal(10000));
		transactionTwo.setDateTime(new Timestamp(System.currentTimeMillis()));
		transactionList.add(transaction);
		transactionList.add(transactionTwo);

		Mockito.when(transactionalRepository.findByAccountNumber(Mockito.anyLong()))
				.thenReturn(Optional.of(transactionList));
		transactionService.getAllTransactionByAccountNumber(1L);

		Assert.assertEquals(transactionService.getAllTransactionByAccountNumber(1L).size(), transactionList.size());

	}

}
