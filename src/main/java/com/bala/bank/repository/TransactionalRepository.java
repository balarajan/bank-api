package com.bala.bank.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bala.bank.model.Transaction;

@Repository
public interface TransactionalRepository extends JpaRepository<Transaction, Long> {

	@Query("SELECT t FROM Transaction t WHERE t.sourceAccountNumber = :accountNumber")
	Optional<List<Transaction>> findByAccountNumber(@Param("accountNumber") Long accountNumber);

}
