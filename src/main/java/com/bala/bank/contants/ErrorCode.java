package com.bala.bank.contants;

/**
 * The ErrorCode Class.
 */
public class ErrorCode {

	/**
	 * Error Code for any error related to account.
	 */
	public static final String ACCOUNT_ERROR_CODE = "ERROR_001";

	/**
	 * Error Code for error related to money transaction.
	 */
	public static final String TRANSACTION_ERROR = "ERROR_002";

	/**
	 * Error Code related to insufficient balance.
	 */
	public static final String CLIENT_ERROR = "ERROR_003";

}
