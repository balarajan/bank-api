package com.bala.bank.exception;

/**
 * 
 * The InsufficientBalanceException class.
 *
 */
public class InsufficientBalanceException extends ApplicationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5789657682447401663L;

	/**
	 * ApplicationException constructor with arguments.
	 * 
	 * @param message   The error message
	 * @param errorCode The error code
	 */
	public InsufficientBalanceException(String message, String errorCode) {
		super(message, errorCode);
	}
}
