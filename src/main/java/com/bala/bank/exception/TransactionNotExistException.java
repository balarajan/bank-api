package com.bala.bank.exception;

import org.springframework.http.HttpStatus;

/**
 * 
 * The TransactionNotExistException class.
 *
 */
public class TransactionNotExistException extends ApplicationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -635094015725770590L;

	/**
	 * TransactionNotExistException constructor with arguments.
	 * 
	 * @param message   The error message
	 * @param errorCode The error code
	 */
	public TransactionNotExistException(String message, String errorCode) {
		super(message, errorCode);
	}

	/**
	 * TransactionNotExistException constructor with arguments
	 * 
	 * @param message    The error message
	 * @param errorCode  The error code
	 * @param httpStatus The HTTP Status
	 */
	public TransactionNotExistException(String message, String errorCode, HttpStatus httpStatus) {
		super(message, errorCode, httpStatus);
	}

}
