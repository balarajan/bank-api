package com.bala.bank.exception;

import org.springframework.http.HttpStatus;

/**
 * The AccountNotExistException class.
 *
 */
public class AccountNotExistException extends ApplicationException{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2767125037861613923L;

	/**
	 * AccountNotExistException constructor with arguments.
	 * @param message The error message
	 * @param errorCode The error code
	 */
	public AccountNotExistException(String message, String errorCode) {
		super(message, errorCode);
	}
	
	/**
	 * AccountNotExistException constructor with arguments.
	 * @param message The error message
	 * @param errorCode The error code
	 * @param httpStatus The HTTP status
	 */
	public AccountNotExistException(String message, String errorCode, HttpStatus httpStatus) {
		super(message, errorCode, httpStatus);
	}

}
