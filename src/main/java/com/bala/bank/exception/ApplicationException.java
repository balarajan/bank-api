package com.bala.bank.exception;

import org.springframework.http.HttpStatus;

/**
 * The ApplicationException class.
 *
 */
public class ApplicationException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 812660454706554801L;

	private final String errorCode;

	private final HttpStatus httpStatus;

	/**
	 * ApplicationException constructor with arguments.
	 * @param message The error message
	 * @param errorCode The error code
	 */
	public ApplicationException(String message, String errorCode) {
		super(message);

		this.errorCode = errorCode;
		this.httpStatus = HttpStatus.BAD_REQUEST;
	}

	/**
	 * ApplicationException constructor with arguments.
	 * @param message The error message
	 * @param errorCode The error code
	 * @param httpStatus The HTTP Status
	 */
	public ApplicationException(String message, String errorCode, HttpStatus httpStatus) {
		super(message);

		this.errorCode = errorCode;
		this.httpStatus = httpStatus;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}
}