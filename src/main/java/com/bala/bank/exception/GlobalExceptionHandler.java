package com.bala.bank.exception;

import java.util.Collections;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import com.bala.bank.contants.ErrorCode;
import com.bala.bank.dto.ErrorResponse;

/**
 * 
 * The GlobalExceptionHandler class.
 *
 */
@ControllerAdvice
public class GlobalExceptionHandler {
	private static final Logger log = LoggerFactory.getLogger(GlobalExceptionHandler.class);

	/**
	 * Method handles the exception specified in @ExceptionHandler annotation.
	 * 
	 * @param req The HttpServletRequest Object
	 * @param e   The Exception Object
	 * @return The Response Entity Object.
	 */
	@ExceptionHandler(value = { BindException.class, MethodArgumentNotValidException.class,
			MethodArgumentTypeMismatchException.class, HttpMessageNotReadableException.class,
			HttpRequestMethodNotSupportedException.class, HttpMediaTypeNotSupportedException.class })
	@ResponseBody
	public ResponseEntity<ErrorResponse> validationException(HttpServletRequest req, Exception e) {
		log.error(e.getMessage());

		String errorMsg = (e.getMessage() == null) ? e.getClass().getSimpleName() : e.getMessage();

		ErrorResponse response = new ErrorResponse();
		response.setErrorCode(ErrorCode.CLIENT_ERROR);
		response.setErrorMessage(errorMsg);

		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
	}

	/**
	 * Method handles the exception specified in @ExceptionHandler annotation.
	 * 
	 * @param req The HttpServletRequest Object
	 * @param e   The Exception Object
	 * @return The Response Entity Object.
	 */
	@ExceptionHandler(Exception.class)
	public ResponseEntity<Map> handleException(HttpServletRequest req, Exception e) {
		log.error(e.getMessage(), e);

		String errorMsg = (e.getMessage() == null) ? e.getClass().getSimpleName() : e.getMessage();
		Map<String, Object> error = Collections.singletonMap("error", errorMsg);

		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
	}

	/**
	 * Method handles the exception specified in @ExceptionHandler annotation.
	 * 
	 * @param req The HttpServletRequest Object
	 * @param e   The Exception Object
	 * @return The Response Entity Object.
	 */
	@ExceptionHandler(ApplicationException.class)
	public ResponseEntity<ErrorResponse> handleBusinessException(HttpServletRequest req, Exception e) {
		ApplicationException businessEx = (ApplicationException) e;

		String errorMsg = (e.getMessage() == null) ? e.getClass().getSimpleName() : e.getMessage();
		log.error("ErrorCode: " + businessEx.getErrorCode() + " Error Message: " + businessEx.getMessage());

		ErrorResponse response = new ErrorResponse();
		response.setErrorCode(businessEx.getErrorCode());
		response.setErrorMessage(errorMsg);

		return ResponseEntity.status(businessEx.getHttpStatus()).body(response);
	}

}
