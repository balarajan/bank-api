package com.bala.bank.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bala.bank.contants.ErrorCode;
import com.bala.bank.exception.AccountNotExistException;
import com.bala.bank.exception.ApplicationException;
import com.bala.bank.model.Account;
import com.bala.bank.repository.AccountRepository;
import com.bala.bank.service.AccountServiceI;

/**
 * The AccountServiceImpl is a implementation class to do create, update and get
 * balance operations.
 *
 */
@Service
public class AccountServiceImpl implements AccountServiceI {

	@Autowired
	private AccountRepository accountRepository;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Account saveAccount(Account account) {
		if (accountRepository.findById(account.getAccountNumber()).isPresent()) {
			throw new ApplicationException(
					"Account with id:" + account.getAccountNumber() + " exist. Please use PUT method update balance",
					ErrorCode.ACCOUNT_ERROR_CODE);
		}
		return accountRepository.save(account);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Account updateBalance(Long accountNumber, Account account) {
		Account currentAccountBalance = accountRepository.findById(accountNumber)
				.orElseThrow(() -> new AccountNotExistException(
						"Account with id:" + account.getAccountNumber() + " does not exist.",
						ErrorCode.ACCOUNT_ERROR_CODE));
		currentAccountBalance.setAmount(currentAccountBalance.getAmount().add(account.getAmount()));

		return accountRepository.save(currentAccountBalance);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Account getBalance(Long accountNumber) {
		Account account = accountRepository.findById(accountNumber)
				.orElseThrow(() -> new AccountNotExistException("Account with id:" + accountNumber + " does not exist.",
						ErrorCode.ACCOUNT_ERROR_CODE));
		return account;
	}

}
