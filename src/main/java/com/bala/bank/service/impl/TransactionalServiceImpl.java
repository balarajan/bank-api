package com.bala.bank.service.impl;

import java.sql.Timestamp;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bala.bank.contants.ErrorCode;
import com.bala.bank.exception.AccountNotExistException;
import com.bala.bank.exception.InsufficientBalanceException;
import com.bala.bank.exception.TransactionNotExistException;
import com.bala.bank.model.Account;
import com.bala.bank.model.Transaction;
import com.bala.bank.repository.AccountRepository;
import com.bala.bank.repository.TransactionalRepository;
import com.bala.bank.service.TransactionalServiceI;

/**
 * The TransactionalServiceImpl is a implementation class to do save and get
 * transaction details.
 *
 */
@Service
public class TransactionalServiceImpl implements TransactionalServiceI {

	@Autowired
	private AccountRepository accountRepository;

	@Autowired
	private TransactionalRepository transactionalRepository;

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public Transaction moneyTransfer(Transaction transaction)
			throws AccountNotExistException, InsufficientBalanceException {
		Account sourceAccount = accountRepository.findById(transaction.getSourceAccountNumber())
				.orElseThrow(() -> new AccountNotExistException(
						"Source Account with id:" + transaction.getSourceAccountNumber() + " does not exist.",
						ErrorCode.ACCOUNT_ERROR_CODE));
		Account destinationAccount = accountRepository.findById(transaction.getDestinationAccountNumber())
				.orElseThrow(() -> new AccountNotExistException(
						"Destination Account with id:" + transaction.getDestinationAccountNumber() + " does not exist.",
						ErrorCode.ACCOUNT_ERROR_CODE));

		if (sourceAccount.getAmount().compareTo(transaction.getAmount()) < 0) {
			throw new InsufficientBalanceException("Account with id:" + sourceAccount.getAccountNumber()
					+ " does not have enough balance to transfer.", ErrorCode.TRANSACTION_ERROR);
		}

		sourceAccount.setAmount(sourceAccount.getAmount().subtract(transaction.getAmount()));
		destinationAccount.setAmount(destinationAccount.getAmount().add(transaction.getAmount()));

		accountRepository.save(sourceAccount);
		accountRepository.save(destinationAccount);

		transaction.setDateTime(new Timestamp(System.currentTimeMillis()));
		transactionalRepository.save(transaction);
		return transaction;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Transaction getTransaction(Long transactionId) {
		Transaction transaction = transactionalRepository.findById(transactionId).orElseThrow(
				() -> new TransactionNotExistException("Transaction id:" + transactionId + " does not exist.",
						ErrorCode.TRANSACTION_ERROR));
		return transaction;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Transaction> getAllTransactionByAccountNumber(Long accountNumber) {
		List<Transaction> transactions = transactionalRepository.findByAccountNumber(accountNumber)
				.orElseThrow(() -> new TransactionNotExistException(
						"No Transactions exist for the account number :" + accountNumber + ".",
						ErrorCode.TRANSACTION_ERROR));
		if (transactions.isEmpty()) {
			throw new TransactionNotExistException(
					"No Transactions exist for the account number :" + accountNumber + ".",
					ErrorCode.TRANSACTION_ERROR);
		}
		return transactions;
	}

}
