package com.bala.bank.service;

import com.bala.bank.model.Account;

/**
 *
 * The AccountServiceI interface.
 */
public interface AccountServiceI {

	/**
	 * Save account details.
	 * 
	 * @param account The Account object
	 * @return account The Account object
	 */
	public Account saveAccount(Account account);

	/**
	 * Update the balance in existing account
	 * 
	 * @param accountNumber The account number
	 * @param account       The Account object
	 * @return account The Account object
	 */
	public Account updateBalance(Long accountNumber, Account account);

	/**
	 * Get the balance of the existing account
	 * 
	 * @param accountNumber The account number
	 * @return account The Account object
	 */
	public Account getBalance(Long accountNumber);

}
