package com.bala.bank.service;

import java.util.List;

import com.bala.bank.exception.AccountNotExistException;
import com.bala.bank.exception.InsufficientBalanceException;
import com.bala.bank.model.Transaction;

/**
 * The TransactionalServiceI Interface.
 *
 */
public interface TransactionalServiceI {

	/**
	 * Transfer money from source account to destination account.
	 * 
	 * @param transaction The Transaction Object
	 * @return transaction The Transaction Object
	 * @throws AccountNotExistException     if account does not exist
	 * @throws InsufficientBalanceException if source account does not have
	 *                                      sufficient balance.
	 */
	public Transaction moneyTransfer(Transaction transaction)
			throws AccountNotExistException, InsufficientBalanceException;

	/**
	 * Get the transaction details by the id of transaction.
	 * 
	 * @param transactionId The transactio Id
	 * @return transaction The Transaction Object
	 */
	public Transaction getTransaction(Long transactionId);

	/**
	 * Get all the transaction details by account number.
	 * 
	 * @param accountNumber The source account number
	 * @return the list of transaction
	 */
	public List<Transaction> getAllTransactionByAccountNumber(Long accountNumber);

}
