package com.bala.bank.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "TRANSACTION")
public class Transaction implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4136675479518192241L;

	/**
	 * The transaction Id
	 */
	@Id
	@GeneratedValue
	@Column(nullable = false, unique = true, updatable = false)
	@ApiModelProperty(notes = "Transaction Id", name = "transactionId", required = false)
	private Long transactionId;

	/**
	 * The Source account number
	 */
	@NotNull(message = "Source Account number is must to transfer money.")
	@Column(nullable = false)
	@ApiModelProperty(notes = "Source Account Number", name = "sourceAccountNumber", required = true)
	private Long sourceAccountNumber;

	/**
	 * The Destination account number
	 */
	@NotNull(message = "Destination Account number is must to transfer money.")
	@Column(nullable = false)
	@ApiModelProperty(notes = "Destination Account Number", name = "destinationAccountNumber", required = true)
	private Long destinationAccountNumber;

	/**
	 * The amount
	 */
	@NotNull(message = "Amount should not be Null.")
	@Min(value = 0, message = "Account balance must be positive.")
	@Column(nullable = false)
	@ApiModelProperty(notes = "Amount to be deposit", name = "amount", required = true)
	private BigDecimal amount;

	/**
	 * The time stamp
	 */
	@Column(nullable = false)
	@ApiModelProperty(notes = "Transaction time", name = "dateTime", required = false)
	private Timestamp dateTime;

	public Long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}

	public Long getSourceAccountNumber() {
		return sourceAccountNumber;
	}

	public void setSourceAccountNumber(Long sourceAccountNumber) {
		this.sourceAccountNumber = sourceAccountNumber;
	}

	public Long getDestinationAccountNumber() {
		return destinationAccountNumber;
	}

	public void setDestinationAccountNumber(Long destinationAccountNumber) {
		this.destinationAccountNumber = destinationAccountNumber;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Timestamp getDateTime() {
		return dateTime;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((amount == null) ? 0 : amount.hashCode());
		result = prime * result + ((dateTime == null) ? 0 : dateTime.hashCode());
		result = prime * result + ((destinationAccountNumber == null) ? 0 : destinationAccountNumber.hashCode());
		result = prime * result + ((sourceAccountNumber == null) ? 0 : sourceAccountNumber.hashCode());
		result = prime * result + ((transactionId == null) ? 0 : transactionId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Transaction other = (Transaction) obj;
		if (amount == null) {
			if (other.amount != null)
				return false;
		} else if (!amount.equals(other.amount))
			return false;
		if (dateTime == null) {
			if (other.dateTime != null)
				return false;
		} else if (!dateTime.equals(other.dateTime))
			return false;
		if (destinationAccountNumber == null) {
			if (other.destinationAccountNumber != null)
				return false;
		} else if (!destinationAccountNumber.equals(other.destinationAccountNumber))
			return false;
		if (sourceAccountNumber == null) {
			if (other.sourceAccountNumber != null)
				return false;
		} else if (!sourceAccountNumber.equals(other.sourceAccountNumber))
			return false;
		if (transactionId == null) {
			if (other.transactionId != null)
				return false;
		} else if (!transactionId.equals(other.transactionId))
			return false;
		return true;
	}

	public void setDateTime(Timestamp dateTime) {
		this.dateTime = dateTime;
	}

}
