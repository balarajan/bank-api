package com.bala.bank.controller;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bala.bank.exception.AccountNotExistException;
import com.bala.bank.model.Account;
import com.bala.bank.service.AccountServiceI;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * The AccountController.
 *
 */
@RestController
@RequestMapping("/account")
@Api(value = "Account Controller")
public class AccountController {

	private static final Logger log = LoggerFactory.getLogger(AccountController.class);

	@Autowired
	public AccountServiceI accountService;

	/**
	 * API to save the account details.
	 * 
	 * @param account The Account Object.
	 * @return account The Account Object.
	 * @throws Exception
	 */
	@ApiOperation(value = "Save the accounts.", response = Account.class, tags = "saveAccount")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success|OK"),
			@ApiResponse(code = 404, message = "Not Found"), @ApiResponse(code = 500, message = "Server Error!!!") })
	@PostMapping
	public ResponseEntity<Account> saveAccount(@Valid @RequestBody Account account) throws Exception {
		try {
			return ResponseEntity.ok(accountService.saveAccount(account));
		} catch (Exception e) {
			log.error("Fail to save the account, please check with system admin team.");
			throw e;
		}
	}

	/**
	 * API to update the amount by existing account.
	 * 
	 * @param accountNumber The account number
	 * @param account       The Account Object
	 * @return The Account object
	 * @throws Exception
	 */
	@ApiOperation(value = "Update the ammount to specific account.", response = Account.class, tags = "updateAccount")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success|OK"),
			@ApiResponse(code = 404, message = "Not Found"), @ApiResponse(code = 500, message = "Server Error!!!") })
	@PutMapping("/{accountNumber}")
	public ResponseEntity<Account> updateAccount(
			@NotNull(message = "Account Number should not be Null.") @PathVariable(value = "accountNumber") Long accountNumber,
			@Valid @RequestBody Account account) throws Exception {
		try {
			return ResponseEntity.ok(accountService.updateBalance(accountNumber, account));
		} catch (AccountNotExistException e) {
			log.error("Account does not exist, please check with system admin team.");
			throw e;
		}
	}

	/**
	 * API to get the balance of amount.
	 * 
	 * @param accountNumber The account number
	 * @return The Account object
	 * @throws Exception
	 */
	@ApiOperation(value = "Get the balance from specific account.", response = Account.class, tags = "getBalance")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success|OK"),
			@ApiResponse(code = 404, message = "Not Found"), @ApiResponse(code = 500, message = "Server Error!!!") })
	@GetMapping("/{accountNumber}")
	public ResponseEntity<Account> getBalance(
			@NotNull(message = "Account Number should not be Null.") @PathVariable(value = "accountNumber") Long accountNumber)
			throws Exception {
		try {
			return ResponseEntity.ok(accountService.getBalance(accountNumber));
		} catch (AccountNotExistException e) {
			log.error("Account does not exist, please check with system admin team.");
			throw e;
		}
	}

}
