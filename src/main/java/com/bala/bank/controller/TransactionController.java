package com.bala.bank.controller;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bala.bank.exception.AccountNotExistException;
import com.bala.bank.exception.InsufficientBalanceException;
import com.bala.bank.model.Transaction;
import com.bala.bank.service.TransactionalServiceI;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * The TransactionController.
 *
 */
@RestController
@RequestMapping("/transaction")
public class TransactionController {

	private static final Logger log = LoggerFactory.getLogger(TransactionController.class);

	@Autowired
	private TransactionalServiceI transactionService;

	/**
	 * API to do the money transfer and save the transaction.
	 * 
	 * @param transaction The transaction object
	 * @return transaction The transaction object
	 * @throws Exception
	 */
	@ApiOperation(value = "Save the transaction.", response = Transaction.class, tags = "saveTransaction")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success|OK"),
			@ApiResponse(code = 404, message = "Not Found"), @ApiResponse(code = 500, message = "Server Error!!!") })
	@PostMapping
	public ResponseEntity<Transaction> saveTransaction(@Valid @RequestBody Transaction transaction) throws Exception {
		try {
			return ResponseEntity.ok(transactionService.moneyTransfer(transaction));
		} catch (AccountNotExistException | InsufficientBalanceException e) {
			log.error("Fail to transfer money the account, please check with system admin team.");
			throw e;
		}
	}

	/**
	 * API to get the transaction details by transaction id.
	 * 
	 * @param transactionId The Transaction Id
	 * @return transaction The transaction object
	 * @throws Exception
	 */
	@ApiOperation(value = "Get the transaction.", response = Transaction.class, tags = "getTransaction")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success|OK"),
			@ApiResponse(code = 404, message = "Not Found"), @ApiResponse(code = 500, message = "Server Error!!!") })
	@GetMapping("/{transactionId}")
	public ResponseEntity<Transaction> getTransaction(
			@NotNull(message = "Transaction Id should not be Null.") @PathVariable(value = "transactionId") Long transactionId)
			throws Exception {
		try {
			return ResponseEntity.ok(transactionService.getTransaction(transactionId));
		} catch (AccountNotExistException e) {
			log.error("Transaction Id does not exist, please check with system admin team.");
			throw e;
		}
	}

	/**
	 * API to get all the transaction details by source account number.
	 * 
	 * @param accountNumber The source account number.
	 * @return transaction The transaction object
	 * @throws Exception
	 */
	@ApiOperation(value = "Get the transaction.", response = Iterable.class, tags = "getAllTransactionByAccountNumber")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success|OK"),
			@ApiResponse(code = 404, message = "Not Found"), @ApiResponse(code = 500, message = "Server Error!!!") })
	@GetMapping("/{accountNumber}/account")
	public ResponseEntity<List<Transaction>> getAllTransactionByAccountNumber(
			@NotNull(message = "Account Number should not be Null.") @PathVariable(value = "accountNumber") Long accountNumber)
			throws Exception {
		try {
			return ResponseEntity.ok(transactionService.getAllTransactionByAccountNumber(accountNumber));
		} catch (AccountNotExistException e) {
			log.error("Account does not exist, please check with system admin team.");
			throw e;
		}
	}

}
