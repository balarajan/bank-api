Bank-Api
====================================================================================================================================
Spring boot application which provide RESTful API for Save account and Money transfer services.

Prerequisite

    1.	Maven
    2.	JDK 1.8+
	
Project Structure

bank-api
│   ├── pom.xml
├── src
│   ├── main
│   │   ├── java
│   │   │   └── com
│   │   │       └── bala
│   │   │           └── bank
│   │   │               ├── BankServiceApplication.java
│   │   │               ├── config
│   │   │               │   └── SwaggerConfig.java
│	│	│				├── constants
│	│	│				│	└── ErrorCode.java
│   │   │               ├── controller
│   │   │               │   ├── AccountController.java
│   │   │               │   └── TransactionController.java
│   │   │               ├── dto
│   │   │               │	└── ErrorResponse.java
│   │   │               ├── exception
│   │   │               │   ├── AccountNotExistException.java
│	│	│				│	├── ApplicationException.java
│	│	│				│	├── GlobalExceptionHandler.java
│	│	│				│	├── InsufficientBalanceException.java
│   │   │               │   └── TransactionNotExistException.java
│   │   │               ├── entity
│   │   │               │   ├── Account.java
│   │   │               │   └── Transaction.java
│   │   │               ├── repository
│   │   │               │   ├── AccountRepository.java
│   │   │               │   └── TransactionalRepository.java
│   │   │               └── service
│   │   │                   ├── AccountServiceI.java
│	│	│					├── TransactionalServiceI.java
│   │   │                   └── impl
│	│	│						├──	AccountServiceImpl.java
│   │   │                       └── TransactionalServiceImpl.java
│   │   └── resources
│   │       ├── application.properties
│   └── test
│       └── java
│           └── com
│               └── bala
│                   └── bank
│                       └── service
│                           └── impl
│								├──	AccountServiceImplTest.java
│                               └── TransactionalServiceImplTest.java


Packaging: Build jar and run the junit classes
mvn clean install

Run the jar:
java -jar bank-api-0.0.1.jar

Swagger-UI
http://localhost:8080/swagger-ui.html


API Information	
--------------|-----------------------------------------------------|-----------------------------------|---------------------------|		
HTTP Method   | 					API								|		Request Body/Path Param		| 		Comments			|
--------------|-----------------------------------------------------|-----------------------------------|---------------------------|					
POST		  | http://localhost:8080/account						|	{								|							|	
			  |														|		"accountNumber":1,			|	To create/save account	|
			  |														|		"amount":8000				|							|
			  |														|	}								|							|
--------------|-----------------------------------------------------|-----------------------------------|--------------------------	|  
PUT			  | http://localhost:8080/account/{accountNumber}		|	{								|							|
			  |														|		"amount":8000				|	To update balance amount|
			  |														|									|	with existing account	|
			  |														|	}								|							|
--------------|-----------------------------------------------------|-----------------------------------|---------------------------|
GET			  | http://localhost:8080/account/{accountNumber}		|	accountNumber should be passed 	|							|
			  |														|	as pathparam					|	To retrive the balance  |
			  |														|									|	amount from	account	    |
			  |														|									|							|
--------------|-----------------------------------------------------|-----------------------------------|---------------------------|
POST		  | http://localhost:8080/transaction					|	{								|							|
			  |														|	"sourceAccountNumber": 1,		|	To transfer money, then |
			  |														|	"destinationAccountNumber": 2,	|	save the transaction	|
			  |														|	"amount": 5000					|							|
			  |														|	}								|							|
--------------|-----------------------------------------------------|-----------------------------------|---------------------------|
GET			  | http://localhost:8080/transaction/{transactionId}	|	transactionId should be passed 	|							|
			  |														|	as pathparam					|	To retrive the details  |
			  |														|									|	of transaction		    |
			  |														|									|							|
--------------|-----------------------------------------------------|-----------------------------------|---------------------------|
GET			  | http://localhost:8080/transaction/{accountNumber}	|	accountNumber should be passed	|	To retrieve the complete|							
			  |	/account											|	as pathparam 					|	deatils of transaction	|
			  |														|									|	for a specific account  |
			  |														|									|	Number	    			|
			  |														|									|							|
--------------|-----------------------------------------------------|-----------------------------------|---------------------------|

Error Codes
--------------------|-----------------------------------------------------|
		Code   		| 					Description						  |
--------------------|-----------------------------------------------------|
ERROR_001		  	| Error Code for any error related to account.		  |
--------------------|-----------------------------------------------------|
TRANSACTION_ERROR	| Error Code for error related to money transaction.  |
--------------------|-----------------------------------------------------|
CLIENT_ERROR		|	   Error Code related to insufficient balance.	  |	
--------------------|-----------------------------------------------------|